package main

import "fmt"

type Queue struct {
	arr   []int
	size  int // 队列容量
	front int
	rear  int
	num   int // 当前存储数量
}

// 初始化一个容量为k的队列
func InitialQueue(k int) Queue {
	var ret Queue
	ret.arr = make([]int, k)
	ret.size = k
	ret.front = 0
	ret.rear = -1
	ret.num = 0
	return ret
}

func (this *Queue) EnQueue(val int) bool {
	if this.IsFull() == true {
		return false
	}

	rear := (this.rear + 1) % this.size
	this.arr[rear] = val
	this.rear = rear
	this.num = this.num + 1
	return true
}

func (this *Queue) DeQueue() bool {
	if this.IsEmpty() == true {
		return false
	}

	this.front = (this.front + 1) % this.size
	this.num = this.num - 1
	return true
}

func (this *Queue) Front() int {
	if this.IsEmpty() == true {
		return -1
	} else {
		return this.arr[this.front]
	}
}

func (this *Queue) Rear() int {
	if this.IsEmpty() == true {
		return -1
	} else {
		return this.arr[this.rear]
	}
}

func (this *Queue) IsEmpty() bool {
	return this.num == 0
}

func (this *Queue) IsFull() bool {
	return this.num == this.size
}

func main() {
	q := InitialQueue(10)
	q.EnQueue(9)
	q.EnQueue(3)
	fmt.Println(q.Front(), q.Rear())
	q.DeQueue()

	fmt.Println(q.Front(), q.Rear())
}
