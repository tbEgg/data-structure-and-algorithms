package main

import "fmt"

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func preorderTraversal(root *TreeNode) []int {
	ret := make([]int, 0)
	if root == nil {
		return ret
	}

	cur := root
	stack := make([]*TreeNode, 0)
	s := -1

	for true {
		for cur != nil {
			ret = append(ret, cur.Val)
			if cur.Right != nil {
				stack = append(stack, cur)
				s = s + 1
			}
			cur = cur.Left
		}

		if s < 0 {
			break
		}

		cur = stack[s].Right
		stack = stack[:s]
		s = s - 1
	}
	return ret
}

func inorderTraversal(root *TreeNode) []int {
	ret := make([]int, 0)
	if root == nil {
		return ret
	}

	cur := root
	stack := make([]*TreeNode, 0)
	s := -1

	for true {
		for cur != nil {
			stack = append(stack, cur)
			s = s + 1
			cur = cur.Left
		}

		if s < 0 {
			break
		}

		cur = stack[s]
		ret = append(ret, cur.Val)
		cur = cur.Right
		stack = stack[:s]
		s = s - 1
	}
	return ret
}

func postorderTraversal(root *TreeNode) []int {
	ret := make([]int, 0)
	if root == nil {
		return ret
	}

	stack := make([]*TreeNode, 1)
	s := 0
	stack[s] = root

	for s >= 0 {
		cur := stack[s]
		stack = stack[:s]
		s = s - 1

		if cur.Left != nil {
			stack = append(stack, cur.Left)
			s = s + 1
		}

		if cur.Right != nil {
			stack = append(stack, cur.Right)
			s = s + 1
		}

		ret = append([]int{cur.Val}, ret...)
	}

	return ret
}

func main() {
	fmt.Println("vim-go")
}
